# README #

### What is this repository for? ###

* Removes Completed or Stopped torrents in Transmission Server
* 1.0

### How do I get set up? ###

* edit config.py to suit your transmission server environment
* Dependencies - transmissionrpc
* Setup a cron on the server hosting the transmission instance to run tc.py file.

### Contribution guidelines ###

Welcome to make it better!

### Who do I talk to? ###

PM me here!