import transmissionrpc
import config

HOST = config.HOST
PORT = config.PORT
USER = config.USER
PASSWORD = config.PASSWORD

"""
I use this as a cron job to remove any torrents that have been completed, either 'stopped' by other means or 'seeding'.
It was specifically written so it doesnt remove any paused torrents as they appear under the status of 'stopped'.

"""
tc = transmissionrpc.Client(HOST, port = PORT, user = USER, password = PASSWORD, timeout="10")
for t in tc.get_torrents():
    a= t.files() #get a dict with all the files associated with the torrent
    c = True # set counter to True, will spoil to false if any torrent has not fully downloaded.
    for b in a: #for each torrent in transmission presently
        if a[b]['completed'] == a[b]['size']: #check the size of each file inside the torrent and compare with completed parts
            c = c * True
        else:
            c = c * False
    if c: #if it has been 'completed' and size and completed labels match then it is finished and can be removed
        if (t.status == 'stopped') | (t.status == 'seeding'):
            tc.remove_torrent(t.id)
            print "removed %s" % t.id


